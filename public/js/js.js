/**
 * Created by User on 23.08.2016.
*/


$(document).ready(function() {
    $('#autoWidth').lightSlider({
        item:1,
        loop:false,
        slideMove:1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:700,
        pager:false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:1,
                    slideMove:1,
                    slideMargin:3,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });

    var secondSlider = $("#demo").lightSlider({
        item: 1,
        loop: true,
        pager: false,
        controls: false,
        enableDrag: false,
        auto: true,
        pause: 6000
    });
    $('.button_prev').click(function(){
        secondSlider.goToPrevSlide();
    });
    $('.button_next').click(function(){
        secondSlider.goToNextSlide();
    });
});
