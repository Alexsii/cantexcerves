ymaps.ready(function () {
    var address = $('ul.addresses li:first-child').text();
    var map;

    ymaps.geocode(address).then(function (res) {

        var coords = res.geoObjects.get(0).geometry.getCoordinates();
        map = new ymaps.Map('map', {
            center: coords,
            zoom: 13
        });

        var placemark = new ymaps.Placemark(coords, {
            balloonContent: 'Сантехсервис\n' + address,
        }, {
            preset: "twirl#yellowStretchyIcon",
        });
        map.geoObjects.add(placemark);
    });
});
