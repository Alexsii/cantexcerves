@extends('welcome')
@section('styles')
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{ url('css/products.css')}}" rel="stylesheet">
    <link href="{{ url('css/logo_styles.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div>
        <a class="social_network"></a>
    </div>
    <div class="case_product">
        <div class="box_product">
          @foreach($categoriess as $categories)
            <a href="{{ url('products/air/'.$categories->categories_id) }}">
           <div class="forums_box">
               <div class="sprite_box">
                   <div class="sprite sprite_{{ $categories->id }}"></div>
               </div>

               <div class="sprite_text">
                   {{ $categories->title }}
               </div>
           </div>
            </a>
              @endforeach
        </div>
    </div>
    <div class="forums">
        <div class="forums_case">
            {!! Breadcrumbs::render('products') !!}
        </div>
    </div>
    @foreach($products as $product)
        <div class="content_box">
            <div class="content_case">
                <div class="content_text_box">
                    <div class="header">
                        <h3>{{ $product->title }}</h3>
                    </div>

                    <div class="header_text">
                        {{ $product->body }}
                    </div>

                    <div class="button_box">
                        <a href="#" class="button">Подробней</a>
                    </div>
                </div>

                <div class="content_img_box">
                    <div class="img_case">
                        <img style="max-width:340px; max-height: 380px;" src="{{ $product->images }}">
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#sloo").fadeIn(1000);
        })
    </script>

@endsection
@endsection