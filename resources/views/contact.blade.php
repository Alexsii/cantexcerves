@extends('welcome')
<!-- start slider ------------------------------->
@section('styles')
    <mstyle>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ url('css/products.css')}}" rel="stylesheet">
    </mstyle>
@endsection
@section('content')
    <div class="forums">
        <div class="forums_case">
            {!! Breadcrumbs::render('contact') !!}
        </div>
    </div>
    <!-- end slider ------------------------------->
    <!-- yandex ------------------------------->
   <div>
    <div class="box_maps">
        @if($addres != "")
        <div style="visibility: hidden; margin: 0;padding: 0; height: 0;">
                {{ $addres }}

        </div>
        <ul class="addresses" style="visibility: hidden; margin: 0; padding: 0px; height: 0;">
                <li>{{ $addres }} </li>
        </ul>
        <div id="map" style="width: 100%; height: 100%;"></div>
    </div>
       @else
           <p style="text-align: center; font-size: 24px; color:#002b80;">Параметр адреса отсутсвует!<br>Приносим наши извинения</p>
           <h5 style="text-align: center">Если есть вопросы обратитесь к администрации через операцию "Обратная связь" в шапке сайта!</h5>
       @endif
   </div>
    <!-- end yandex ------------------------------->
@endsection
@section('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/maps.js"></script>
@endsection