@extends('welcome')
<!-- start slider ------------------------------->
@section('styles')
    <mstyle>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ url('css/logo_styles.css') }}" rel="stylesheet">
        <link href="{{ url('css/lightslider.css') }}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
        <script src="{{ url('js/lightslider.js') }}" type="text/javascript"></script>
        <script src="{{ url('js/js.js') }}" type="text/javascript"></script>
    </mstyle>
@endsection
@section('content')
    <div class="slider_box">
        <div class="demo">
            <div class="item">
                <ul id="autoWidth" class="cs-hidden">
                    @foreach($sliders as $slider)
                        <li>
                          <img src="{{ $slider->pach }}" class="slider_images">
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <!-- end slider ------------------------------->
    <!-- content ------------------------------->
    @foreach($products as $product)
        <div class="content_box">
            <div class="content_case">
                <div class="content_text_box">
                    <div class="header">
                        <h3>{{ $product->title }}</h3>
                    </div>

                    <div class="header_text">
                        {{ $product->body }}
                    </div>

                    <div class="button_box">
                        <a href="#" class="button">Подробней</a>
                    </div>
                </div>

                <div class="content_img_box">
                    <div class="img_case">
                        <img style="max-width:340px; max-height: 380px;" src="{{ $product->images }}">
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <!-- end content ------------------------------->
@endsection