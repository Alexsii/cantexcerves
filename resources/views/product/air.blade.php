@extends('welcome')
@section('styles')
    <link href="{{ url('css/product_cases.css')}}" rel="stylesheet">
@endsection
@section('content')

    <div class="case_product">
        <div class="box_product">
            @foreach($categoriess as $categories)
                <a href="{{ url('products/air/'.$categories->categories_id) }}">
                    <div class="forums_box">
                        <div class="sprite_box">
                            <div class="sprite sprite_{{ $categories->id }}"></div>
                        </div>

                        <div class="sprite_text">
                            {{ $categories->title }}
                        </div>
                    </div>
                </a>
                @endforeach
            </a>

        </div>
    </div>
    <div class="forums">
        <div class="forums_case">
            {!! Breadcrumbs::render($ids) !!}
        </div>
    </div>
    @if(!$products->isEmpty())
@foreach($products as $product)
    <div class="content_box">
        <div class="content_case">
            <div class="content_text_box">
                <div class="background">
                <div class="headers">
                    <h3>{{ $product->title }}</h3>
                </div>

                <div class="headers_text">
                    {{ $product->body }}
                </div>
                </div>
                <div class="button_box">
                    <a href="#" class="button">Подробней</a>
                </div>
            </div>

            <div class="content_img_box">
                <div class="img_case">
                    <img src="{{ $product->images }}">
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @else
        <h2 style="text-align: center">Описания товара в этой категории отсуцтвует<br>Просим прощения</h2>
        <h5 style="text-align: center">Если есть вопросы обратитесь к администрации через операцию "Обратная связь" в шапке сайта!</h5>
    @endif
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script>

    </script>
@endsection