<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <link href="{{ url('css/welcome.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body id="sloo" class="body_box">
<div class="hidden">
    <div class="logo_and_contact_box">

      <a href="/"><div class="logo_photo">
              @foreach($logos as $logo)
                 <img src="{{ $logo->logo }}" alt="photo logo" width="390px" hidden="70px">
              @endforeach
        </div></a>

        <div class="phone_box">
            <ul class="phone_img">
                <li><img src="{{ url('images/phone.png') }}" alt="phone"></li>
            </ul>

            <ul class="phone_number" >
                @foreach($numbers as $number)
                    <li class="fa-mobile-phone">{{ $number->phone }}</li>
                @endforeach
                        <a class="call" href="javascript:void(0)" onclick = "document.getElementById('envelope').style.display='block';document.getElementById('fade').style.display='block'">
                            Обратная связь
                        </a>

                    <div id="envelope" class="envelope">

                        <a class="close-btn" href="javascript:void(0)" onclick = "document.getElementById('envelope').style.display='none';document.getElementById('fade').style.display='none'">
                            Закрыть
                        </a>

                        <h1 class="title">
                            Отправить нам сообщение
                        </h1>

                    {!! Form::open(['url' => '/stor', 'method' => 'post']) !!}

                        {!! Form::text('name_users',null,array('class' => 'your-name','placeholder'=>'* Ваше Имя')) !!}
                        {!! Form::email('email_users',null , array('class' => 'email-address', 'placeholder'=>'* Ваш E-mail')) !!}
                        <div style="visibility: hidden; height: 0; width: 0; margin: 0;">
                            {!! Form::text('status','0', ['class' => 'form-control']) !!}
                        </div>
                        {!! Form::textarea('messages',null,['class' => 'your-message',  "cols" => '10' ,'wrap'=>"hard",'placeholder'=>'Пожалуйста, введите своё сообщение здесь ..']) !!}
                        {!! Form::submit('Отправить', ['class' => 'send-message']) !!}

                    {!! Form::close() !!}
                    </div>
                    <div id="fade" class="black-overlay"></div>

            </ul>
        </div>

        <div class="gps_box">
            @if($adds != '')
            <ul class="gps_maps_img">
                <a href="https://www.google.ru/maps/place/@foreach($address as $addres){{ $addres->addres }}@endforeach" target="_blank">
                    <li class="gps_img"><img src="{{ url('images/gps.png') }}"></li>
                    <li class="gps_maps">@foreach($address as $addres){{ $addres->addres }}@endforeach</li></a>
            </ul>
            @endif
            @if($emal != '')
            <ul class="case_email_img">
                <li class="email"><img src="{{ url('images/email.png') }}"></li>
            </ul>

            <ul class="case_email">
                @foreach($emails as $email)
                <a href="https://accounts.google.com" target="_blank"><li class="email">{{ $email->email }}</li></a>
                @endforeach
            </ul>
               @endif
        </div>
    </div>
</div>

<div class="menu_box">
    <div class="menu_case">
        <ul class="case_list">
            <a href="/"><li class="list">Главная</li></a>
            <a href="#"><li class="list">Технология</li></a>
            <a href="#"><li class="list">Объекты</li></a>
            <a href="{{ url('/products') }}"><li class="list">Продукция</li></a>
            <a href="#"><li class="list">История тепловых насосов</li></a>
            <a href="#"><li class="list">Цены</li></a>
            <a href="{{ url('/contact') }}"><li class="list">Контакты</li></a>
        </ul>
    </div>
</div>
@yield('content')
<section>
  <ul class="social_network">
      @foreach($socials as $social)
          @if($social->https != '')
    <a href="{{ $social->https }}"><li class="social"><img src="{{ $social->social }}"></li></a>
          @endif
    @endforeach
  </ul>
</section>
<section>
  <p id="back-top">
      <a href="#top"><span>Вернуться на верх</span></a>
  </p>
</section>
<!-- footer ------------------------------->
<div class="footer">
    <div class="footer_box">
        <div class="footer_text">
            2015 ”Сантехсервис”. Все права защищены.
        </div>

        <div class="author">
            <p>Создание сайта:</p><a href="#"><img src="{{ url('images/dvacom.png') }}"></a>
        </div>
    </div>
</div>
@yield('scripts')
<script type="text/javascript" src="{{ url('js/back_top.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#sloo").fadeIn(1000);
    })
</script>
<!-- end footer ------------------------------->
</body>
</html>