@extends('admin.index')
@section('content')
    <main>

        {!! Form::model($slider, [
                'method' => 'PATCH',
                'url' => '/admin/slider/'.$slider->id,
                'enctype'=> 'multipart/form-data'
             ]) !!}
            <div class="form-group">
                {!! Form::label('email', 'File:') !!}
                {!! Form::file('pach', ['class' => 'form-control', 'required']) !!}
            </div>
                 {!! Form::submit('Добавить фото в слайдер', ['class' => 'form-control']) !!}
        {!! Form::close() !!}

    </main>
@endsection