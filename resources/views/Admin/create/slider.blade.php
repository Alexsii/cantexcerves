@extends('admin.index')
@section('styles')
    <link href="{{url('/master/css/product_form.css') }}" rel="stylesheet">
@endsection
@section('content')
<main>
    <div class="img_box">
        @if($slid != null)
@foreach($sliders as $slider)
    <li class="images_box">
        <a href="{{ route('admin.slider.edit',['slider' => $slider]) }}">
            <img src="{{ url($slider->pach) }}" class="images_case">
        </a>
        <hr/>
        <a href="{{ url('/admin/slider/'.$slider->id.'/destroy') }}" style="text-decoration: none">
            <div class="edit_box">
                Удалить фотографию в слайде
            </div>
        </a>
    </li>
@endforeach
            @else
            <h1>Фото в слайдере отсутствуют</h1>
            @endif
        </div>
        <hr/>
    {!! Form::open(['url' => '/admin/slider', 'enctype'=> 'multipart/form-data']) !!}
      <div class="form-group">
        {!! Form::label('email', 'File:') !!}
        {!! Form::file('pach', ['class' => 'form-control', 'required']) !!}
        </div>
        {!! Form::submit('Create photo for slider', ['class' => 'form-control']) !!}
    {!! Form::close() !!}
</main>
@endsection