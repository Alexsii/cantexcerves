<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="{{ url('/master/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/jquery.navobile.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/style.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <link href="{{ url('/master/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/bootstrap-table.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/check.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/chosen.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/jquery.jgrowl.min.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/jquery.navobile.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/normalize.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/style.css') }}" rel="stylesheet">
    <link href="{{ url('/master/css/switchery.min.css') }}" rel="stylesheet">
    @yield('styles')

</head>

<body>
<div class="wrapper" id="content">
    <header class="header">
        <nav class="header-nav">
            <ul class="f-left">
                <li class="f-left">
                    <a href="#" id="show-sidebar" class="animated slideInLeftSmall"><i class="fa fa-bars fa-2x"></i></a>
                </li>
            </ul>
            <ul class="mainnav f-right">

                <li class={{ Request::segment(2) == 'settings' ? "active" : "" }}>
                    <a href="/admin/settings" data-toggle="tooltip" data-placement="bottom" title="Settings"><i class="fa fa-cog fa-2x"></i></a>
                </li>
                <li class={{ Request::segment(2) == 'messages' ? "active" : "" }}>
                    <a href="/admin/messages" data-toggle="tooltip" data-placement="bottom" title="Notivications"><i class="fa fa-bell fa-2x"></i>
                        @if($i != 0)
                        <span class="badge danger">
                            {{ $i }}
                        </span>
                            @endif
                    </a>
                </li>
                <li><a href="" data-toggle="tooltip" data-placement="bottom" title="Users"><i class="fa fa-users fa-2x"></i></a></li>
                <li><a href="/logout" data-toggle="tooltip" data-placement="bottom" title="Sign out"><i class="fa fa-sign-out fa-2x"></i></a></li>
            </ul>
        </nav>
    </header><!-- .header-->

    <nav class="left-sidebar">
        <div class="logo"><a href="">Cantex</a></div>
        <ul>
            <li class={{ Request::segment(2) == 'home' ? "active" : "" }}>
                <a href="/admin/home"><i class="fa fa-home fa-2x"></i><span>Home</span></a></li>
            <li class={{ Request::segment(2) == 'slider' ? "active" : "" }}>
                <a href="/admin/slider"><i class="fa fa-image fa-2x"></i><span>Слайдер</span></a></li>
            <li class={{ Request::segment(2) == 'address' ? "active" : "" }}>
                <a href="/admin/address"><i class="fa fa-user fa-2x"></i><span>Адреса</span></a></li>
            <li class={{ Request::segment(2) == 'panels' ? "active" : "" }}>
                <a href="/admin/panels"><i class="fa fa-square-o fa-2x"></i><span>Панель</span></a></li>
            <li class={{ Request::segment(2) == 'categories' ? "active" : "" }}>
                <a href="/admin/categories"><i class="fa fa-gratipay fa-2x "></i><span>Категории продуктов</span></a></li>



            <li><a href="/admin/categories" class="has-dropdown"><i class="fa fa-keyboard-o fa-2x"></i><span>Категории продуктов</span></a>
                <ul class="hidden-dropdown">
                    <li><a href="input-text.blade.php"><i class="fa fa-font fa-2x"></i>Input text</a></li>
                    <li><a href=""><i class="fa fa-check-square fa-2x"></i>Input checkbox</a></li>
                    <li class={{ Request::segment(2) == 'audio' ? "active" : "" }}>
                    <a href="/admin/audio"><i class="fa fa-dot-circle-o fa-2x"></i>Input radio</a></li>
                    <li><a href=""><i class="fa fa-caret-square-o-down fa-2x"></i>Input select</a></li>
                </ul>
            </li>
        </ul>
        <!-- <a href="" class="copyright">Created by Drews</a> -->
    </nav>

    @yield('content')
</div><!-- .wrapper -->
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="{{ url('/master/js/bootstrap.min.js') }}"></script>
<!--<script src="js/modernizer.min.js"></script>-->
<script src="{{ url('/master/js/jquery.navobile.min.js') }}"></script>
<script src="{{ url('/master/js/app.js') }}"></script>

@yield('scripts')
</body>
</html>