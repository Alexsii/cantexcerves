@extends('Admin.index')
@section('styles')
    <link href="{{ url('/master/css/products.css')}}" rel="stylesheet">
@endsection
@section('content')
    <main>
        <div class="categories_box">
            @foreach($categoriess as $categories)
                <a href="{{ url('admin/categories/'.$categories->categories_id) }}"><div>
                        <div class="forums_box">
                            <div class="sprite_box">
                                <div class="sprite sprite_{{ $categories->id }}"></div>
                            </div>

                            <div class="sprite_text">
                                {{ $categories->title }}
                            </div>
                        </div>
                    </div></a>
            @endforeach
        </div>
    </main>
@endsection
