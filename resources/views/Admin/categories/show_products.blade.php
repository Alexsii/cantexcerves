@extends('Admin.index')
@section('styles')
    <link href="{{url('/master/css/product_form.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        @if(!$categors->isEmpty())
        @foreach($categors as $categor)
       <div style="visibility: hidden; position: inherit; height: 0;">
           {{ $id = $categor->id }}
       </div>
        @endforeach
        <div class="form_case">
    @foreach($categors as $categor)
          <div class="box">
              <div class="form_box">
                   {!! Form::model($categor, [
                    'method' => 'PATCH',
                    'url' => '/admin/categories/'.$categor->id,
                     'enctype'=> 'multipart/form-data'
                    ]) !!}
                 <div class="case_destroy">{!! Form::label('title', 'Заглавье', ['class' => 'control-label']) !!}
                 <div class="box_destroy"></div>
                 </div>
                 {!! Form::text('title', null, ['class' => 'form-control']) !!}
                 {!! Form::label('body', 'Описание', ['class' => 'control-label']) !!}
                 {!! Form::text('body', null, ['class' => 'form-control']) !!}
                   <div style="visibility: hidden; position: inherit; height: 0;">
                 {!! Form::text('categories_id', $categor->categories_id , ['class' => 'form-control']) !!}</div>
                 {!! Form::file('images', ['class' => 'form-control']) !!}
                 {!! Form::submit('Edit', ['class' => 'form-control']) !!}

                         <a href="{{ url('/admin/categories/'.$categor->id.'/destroy') }}" style="text-decoration: none">
                              <div class="edit_box">
                                  Удалить информацию о продукте
                              </div>
                         </a>
                 {!! Form::close() !!}
            </div>
          </div>
    @endforeach
        </div>
        @else
            <h2>В этой категории отсутсвуют записи</h2>
        @endif
        <hr/>
<div class="creates_form_box">
    <div class="creates_form_case">
        <h2 style="color: white; background: #2ca02c; text-align: center">Создать новую запись</h2>
        {!! Form::open([
'method' => 'POST',
'url' => '/admin/categories/create_product/'.$categories_id,
 'enctype'=> 'multipart/form-data'
]) !!}
        {!! Form::label('title', 'Заглавье', ['class' => 'control-label', 'style' => 'float:left;']) !!}
        {!! Form::text('title', null, ['class' => 'form-control', 'style' => 'float:left;']) !!}
        {!! Form::label('body', 'Описание', ['class' => 'control-label' , 'style' => 'float:left;']) !!}
        {!! Form::textarea('body', null, ['class' => 'form-control', 'style' => 'float:left;']) !!}
       <div style="padding: 0px; margin: 0px; width: 0px;height: 0px;  visibility: hidden">{!! Form::text('categories_id', $categories_id , ['class' => 'form-control', 'style' => 'float:left;']) !!}</div>
        {!! Form::file('images', ['class' => 'form-control']) !!}

        {!! Form::submit('Create', ['class' => 'form-control']) !!}
        {!! Form::close() !!}
   </div>
</div>
    </main>
@endsection