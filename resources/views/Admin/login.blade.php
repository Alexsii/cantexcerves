<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Styles -->
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/bootstrap.min.css" rel="stylesheet">
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/font-awesome.min.css" rel="stylesheet">
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/jquery.navobile.css" rel="stylesheet">
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/animate.min.css" rel="stylesheet">
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/check.css" rel="stylesheet">
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/switchery.min.css" rel="stylesheet"/>
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/chosen.css" rel="stylesheet"/>
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/jquery.jgrowl.min.css" rel="stylesheet"/>
	<link href="../../../../../../../Users/alexsii/Desktop/admin.panel/css/style.css" rel="stylesheet">
	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/bootstrap.min.js"></script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/bootstrap-filestyle.min.js"> </script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/jquery.navobile.min.js"></script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/switchery.min.js"></script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/chosen.jquery.min.js"></script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/jquery.jgrowl.min.js"></script>
	<script src="../../../../../../../Users/alexsii/Desktop/admin.panel/js/app.js"></script>
</head>
<body>
	<div class="page-container">
		<div class="content-container">
			<div class="login-container">
				<div class="content">
					<div class="panel panel-default login-form">
						<div class="logo"><a href="">Drews</a></div>
						<div class="form-group">
							<div class="alert danger">The username or password you entered is incorrect</div>
						</div>
						<div class="panel-body">
							<form method="POST">
								<div class="form-group"><input class="form-control" type="text" name="username" placeholder="Username"></div>
								<div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>
								<div class="form-group"><input class="btn btn-brand" type="submit" value="Sign in"></div>
							</form>
							<div class="text-center">
								<a href="">Need help?</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer>
		© 2015 <a href="">admin panel</a>
	</footer>
</body>
</html>