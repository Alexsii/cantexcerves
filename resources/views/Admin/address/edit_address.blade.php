@extends('admin.index')
@section('content')

    <main>
        {!! Form::model($addres, [
                'method' => 'PATCH',
                'url' => '/admin/address/'.$addres->id,
             ]) !!}
        {!! Form::label('addres', 'Addres:', ['class' => 'control-label']) !!}
        {!! Form::text('addres', null, ['class' => 'form-control']) !!}

        {!! Form::submit('Редактировать адресс', ['class' => 'form-control']) !!}
        {!! Form::close() !!}
    </main>
@endsection