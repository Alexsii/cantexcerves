@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/address.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main class="background_address">
        <div class="case">
         <div style="visibility: hidden; margin: 0; height: 0; width: 0;">@foreach($address as $addres)
              <div >
                <li>
                    <a href="{{ url('admin/address/'.$addres->id.'/edit_address/') }}">{{ $addres->addres }}</a>
                </li>
              </div>
            @endforeach
            </div>
             <hr/>
                <div class="case_maps">
                    <div id="floating-panel">
                        <input id="address" type="textbox" value="{{ $addres->addres}}">
                        <input id="submit" type="button"  onkeyup="geocodeAddress()" value="Показать адрес">
                    </div>
                    <div id="map"></div>
                </div>
                <div class="case_maps">
                    {!! Form::model($addres, ['method' => 'PATCH', 'url' => '/admin/address/'.$addres->id]) !!}
                    <div class="form-group">
                        {!! Form::label('addres', 'Addres:', ['class' => 'control-label']) !!}
                        {!! Form::text('addres', null, ['class' => 'control-label' , 'id' => 'address']) !!}
                    </div>
                    {!! Form::submit('Редактировать адрес', ['class' => 'form-control', 'id' => 'submit']) !!}

                    {!! Form::close() !!}

                </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVImhhTuznB1-kLHVyyS8HVfO47OAdhtg&callback=initMap&librareis=places"
            async defer type="text/javascript"></script>
    <script>

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom:15,
                center: {lat: 48.397, lng: 32.644}
            });
            var geocoder = new google.maps.Geocoder();

            document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
            });
        }

        function geocodeAddress(geocoder, resultsMap) {
            var address = document.getElementById('address').value;
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location
                    });
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
    </script>
@endsection
