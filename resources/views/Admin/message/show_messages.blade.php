@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/messages.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        <div class="case_messages">
            <div class="box_color">
                <div class="case_color"></div>
    @foreach($viewess as $viewes)
            <div >
                <div class="box">
                    <li class="not_viewe">Отправитель:</li>
                    <li >{{ $viewes->name_users }}</li>
                    <hr/>
                </div>

                <div class="box">
                    <li class="not_viewe">E-mail отправителя:</li>
                    <li >{{ $viewes->email_users }}</li>
                    <hr/>
                </div>
                <div class="box">
                    <li class="not_viewe">Меседж:</li>
                    {!! Form::open() !!}
                     {!! Form::textarea('body', ''.$viewes->messages.'' , ['class' => 'form-control','readonly' => "readonly"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        <hr/>
    @endforeach
            </div>
        </div>
    </main>
@endsection