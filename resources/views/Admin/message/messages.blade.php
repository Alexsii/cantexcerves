@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/messages.css') }}" rel="stylesheet">
@endsection
@section('content')
    <main>
        <div class="row">
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading"><h3 class="panel-title">Новые сообщения</h3></div>
                @if(!$messagess->isEmpty())
        @foreach($messagess as $messages)
           <a href="{{ url('/admin/messages/'.$messages->id.'/show_new_messege') }}">
               <div class="not_viewes">
                    <li class="not_viewe">{{ $messages->name_users  }}</li>
                    <li class="not_viewe">{{ $messages->email_users }}</li>
                    <li class="clip not_viewe">{{ $messages->messages }}</li>
               </div>
           </a>
            <hr/>
        @endforeach
        @else
                    <div class="panel-body">
                        У Вас нету новых сообщений
                    </div>
                </div>
            </div>
        @endif
        </div>
        <hr/>
        @if(!$messags->isEmpty())
        <div  class="row">
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Прочитаные сообщения</h3>
                    </div>
                        @foreach($messags as $messag)
                        <a href="{{ url('/admin/messages/'.$messag->id.'/look_old_messeges') }}">
                            <div class="not_viewes">
                                <li class="not_viewe">{{ $messag->name_users }}</li>
                                <li class="not_viewe">{{ $messag->email_users }}</li>
                                <li class="clip not_viewe">{{ $messag->messages }}</li>
                                <a href="{{ url('/admin/messages/'.$messag->id) }}">
                                <li class="fa fa-trash-o fa-2x" aria-hidden="true"></li></a>
                            </div></a>
                            <hr/>
                        @endforeach
                </div>
            </div>
        </div>
        @endif

    </main>
@endsection

@section('scripts')
    <script>

    </script>
@endsection