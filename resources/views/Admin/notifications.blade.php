<?php include 'header.html';?>

<main>
	<div class="page-header">
		<h1><a href="/index.html" class="back"><i class="fa fa-chevron-circle-left"></i></a>Notifications</h1>
	</div>
	<div class="content">
		<div class="form-group"><a href="https://github.com/stanlemon/jGrowl" target="_bland">https://github.com/stanlemon/jGrowl</a></div>
		
		<div class="row">
			<div class="col-md-2">
				<div class="form-group"><button class="btn btn-default">default</button></div>
				<div class="form-group"><button class="btn btn-brand">brand</button></div>
				<div class="form-group"><button class="btn btn-primary">primary</button></div>
				<div class="form-group"><button class="btn btn-danger">danger</button></div>
				<div class="form-group"><button class="btn btn-success">success</button></div>
				<div class="form-group"><button class="btn btn-warning">warning</button></div>
				<div class="form-group"><button class="btn btn-info">info</button></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
						<div class="alert brand alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat iusto quisquam recusandae sint ?
						</div>
				</div>
				<div class="form-group">
						<div class="alert primary alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis eos, veniam consequuntur ores fugit dolore illum saepe!
						</div>
				</div>
				<div class="form-group">
						<div class="alert danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae sed exercitationem erendis? Sapiente, cupiditate.
						</div>
				</div>
				<div class="form-group">
						<div class="alert success alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea iste quod saepe. ti maiores minima.
						</div>
				</div>
				<div class="form-group">
						<div class="alert warning alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor, at rem sapiente 			
						</div>
				</div>
				<div class="form-group">
					<div class="alert info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit vitae rerum repellat quas Dolorum!
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
						<div class="alert alert-with-icon brand alert-dismissible">
							<i class="alert-icon fa fa-shopping-cart"></i>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat iusto quisquam recusandae sint ?
						</div>
				</div>
				<div class="form-group">
						<div class="alert alert-with-icon primary alert-dismissible">
							<i class="alert-icon fa fa-money"></i>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis eos, veniam consequuntur ores fugit dolore illum saepe!
						</div>
				</div>
				<div class="form-group">
						<div class="alert alert-with-icon danger alert-dismissible">
							<i class="alert-icon fa fa-ban"></i>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae sed exercitationem erendis? Sapiente, cupiditate.
						</div>
				</div>
				<div class="form-group">
						<div class="alert alert-with-icon success alert-dismissible">
							<i class="alert-icon fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea iste quod saepe. ti maiores minima.
						</div>
				</div>
				<div class="form-group">
						<div class="alert alert-with-icon warning alert-dismissible">
							<i class="alert-icon fa fa-exclamation-triangle"></i>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor, at rem sapiente 			
						</div>
				</div>
				<div class="form-group">
					<div class="alert alert-with-icon info alert-dismissible">
						<i class="alert-icon fa fa-info-circle"></i>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit vitae rerum repellat quas Dolorum!
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<script>
		$('.btn-default').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				header: "Default" 
			});
		});
		$('.btn-brand').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				theme: "brand",
				header: "Brand"
			});
		});
		$('.btn-primary').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				theme: "primary",
				header: "Primary" 
			});
		});
		$('.btn-danger').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				theme: "danger",
				header: "Danger" 
			});
		});
		$('.btn-success').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				theme: "success",
				header: "Success" 
			});
		});
		$('.btn-warning').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				theme: "warning",
				header: "Warning" 
			});
		});
		$('.btn-info').click(function(){
			$.jGrowl( "The kitten is awake!", {
				sticky:true,
				theme: "info",
				header: "Info" 
			});
		});
	
</script>
<?php include 'footer.html';?>