@extends('admin.index')
@section('styles')
    <link href="{{ url('/master/css/number.css') }}" rel="stylesheet">
@endsection
@section('content')

    <main>
        <div class="box_setting">
        <div class="settings_box">
            <div class="settings_case">
                <div class="settings">
                     Settings
                </div>
            </div>
        </div>
            @if(!$emailss->isEmpty())
            @foreach($emailss as $emails)
                <li class="email_case">
                 {!! Form::model($emails, [
                   'url' => 'admin/settings/emails/' .$emails->id,
                   'method' => 'PATCH']) !!}

                    <div style="margin: 0 auto; width: 650px">
                        <div class="save_box">
                            {!! Form::submit('', array('class' => 'save')) !!}
                        </div>

                        <div class="email_setting">E-mail {{ $emails->id }}:</div>
                        {!! Form::text('email', null, ['class' => 'text']) !!}
                        </div>
                    {!! Form::close() !!}
                </li>
            @endforeach
            @endif
        <hr/>
            @if(!$numbers->isEmpty())
        @foreach($numbers as $number)
            <li class="email_case">
                {!! Form::model($number, [
                  'url' => 'admin/settings/edit_numbers/'.$number->id,
                  'method' => 'PATCH']) !!}
                    <div style="margin: 0 auto; width: 650px">
                        <div class="save_box">
                            {!! Form::submit('', ['class' => 'save']) !!}
                        </div>

                        <div class="email_setting">Номер {{ $number->id }}:</div>
                        {!! Form::text('phone', null, ['class' => 'text']) !!}
                    </div>
                {!! Form::close() !!}
            </li>
        @endforeach
            @endif
        <hr/>
            @if(!$socials->isEmpty())
        @foreach($socials as $social)
        <li class="email_case">
            {!! Form::model($social, [
            'url' => 'admin/settings/social/' .$social->id,
             'method' => 'PATCH']) !!}
            <div style="margin: 0 auto; width: 650px">
                <div class="save_box">
                    {!! Form::submit('', ['class' => 'save']) !!}
                </div>

                <div class="email_setting">{{ $social->name }}:</div>
                {!! Form::text('https', null, ['class' => 'text']) !!}
            </div>
            {!! Form::close() !!}
        </li>
        @endforeach
            @endif
            <hr/>
            @if(!$logos->isEmpty())
            <div class="logo_case">
            @foreach($logos as $logo)
                <li class="email_case">
                    {!! Form::model($logo, [
                    'url' => 'admin/settings/logo/' .$logo->id,
                     'method' => 'PATCH',
                     'enctype'=> 'multipart/form-data']) !!}
                    <div style="margin: 0 auto; width: 650px">
                        <div class="save_box">
                            {!! Form::submit('', ['class' => 'save']) !!}
                        </div>

                        <div class="email_setting">{{ $logo->name }}:</div>
                        {!! Form::file('logo', ['class' => 'text']) !!}
                        <img src="{{ $logo->logo }}" style="width: 450px;height: 120px;">
                        @include('flash::message')
                    </div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {!! Form::close() !!}
                </li>
            @endforeach
            </div>
                @endif
        </div>
    </main>
@endsection