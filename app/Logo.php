<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded =[
        'id'
    ];
    protected $fillable = [
        'name' , 'logo'
    ];
}
