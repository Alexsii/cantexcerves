<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addres extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'id' , 'addres'
    ];
}
