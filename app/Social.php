<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $guarded =[
        'id'
    ];
    protected $fillable = [
        'https', 'social', 'name'
    ];
}
