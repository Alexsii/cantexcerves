<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'id' , 'email'
    ];
}
