<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone_number extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
        'id' , 'phone'
    ];
}
