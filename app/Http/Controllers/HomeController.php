<?php

namespace App\Http\Controllers;

use App\Addres;
use App\Categories;
use App\Email;
use App\Http\Requests;
use App\Logo;
use App\Message;
use App\Phone_number;
use App\Product;
use App\Slider;
use App\Social;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * HomeController constructor.
     */

    public function __construct()
    {
        $numbers = Phone_number::all();
        $address = Addres::all();
        $add = $address->first();
        $adds = $add['addres'];
        $emails = Email::all();
        $emal = $emails->first();
        $emal = $emal['email'];
        $products = Product::all();
        $categoriess = Categories::all();
        $socials = Social::all();
        $logos = Logo::all();
        view()->share(compact('logos','adds','emal','numbers','address','dad','emails', 'products', 'categoriess','socials'));
    }

    public function home(){
        $products = Product::all();
        $sliders = Slider::all();
        return view('home', compact('sliders','products'));
    }
    public function stor(Request $request){
        $messages = $request->all();
        $messages['pablished_at'] = Carbon::now();
        Message::create($messages);
        return redirect('/');
    }
    public function products(){
        return view('products');
    }
    public function air($categories_id){
        $products = Product::whereCategoriesId($categories_id)->get();
        $ids = $categories_id;
        return view('product.air', compact('products','ids'));

    }
    public function contact(){
        $address = Addres::first(['addres']);
        $addres = $address->addres;

        return view('contact', compact('addres'));
    }

}
