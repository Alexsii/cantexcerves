<?php

namespace App\Http\Controllers\Admin;

use App\Email;
use App\Http\Controllers\Admin\MainController;
use App\Http\Requests\CreatephoneRequest;
use App\Http\Requests\LogoRequest;
use App\Logo;
use App\Phone_number;
use App\Social;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

class SettingsController extends MainController
{
    public function updates($id, Request $request, CreatephoneRequest $request){
        $number = Phone_number::findOrFail($id);
        $number->update($request->all());

        $numbers = Phone_number::all();
        $emailss = Email::all();
        $socials = Social::all();
        $logos = Logo::all();
        return view('admin.setting.settings', compact('logos','numbers','emailss','socials'));
    }
    public function update_emails($id, Request $request){
        $emails = Email::findOrFail($id);
        $emails->update($request->all());

        $socials = Social::all();
        $emailss = Email::all();
        $numbers = Phone_number::all();
        $logos = Logo::all();
        return view('admin.setting.settings', compact('logos','emailss','numbers','socials'));
    }
    public function update_social($id, Request $request){
        $social = Social::findOrFail($id);
        $social->update($request->all());

        $socials = Social::all();
        $emailss = Email::all();
        $numbers = Phone_number::all();
        $logos = Logo::all();
        return view('admin.setting.settings', compact('logos','numbers','emailss','socials'));
    }

    public function update_logo($id){

        if(Input::file('logo') != null) {
            $log = Logo::findOrFail($id);
            File::delete(public_path().$log->logo);
            $slid = Input::file('logo');
            $dir = '/uploads/images/';

            do {
                $filename = str_random(30) . '.' . $slid->getClientOriginalExtension();
            } while (File::exists(public_path() . $dir . $filename));

            Input::file('logo')->move(public_path() . $dir, $filename);
            $log->update(['logo' => $dir . $filename]);

        }else{
            Flash::warning('Выберете файл');
        }
        $socials = Social::all();
        $emailss = Email::all();
        $numbers = Phone_number::all();
        $logos = Logo::all();
        return view('admin.setting.settings', compact('logos','numbers','emailss','socials'));
    }

//    home ------------------------------------- //

    public function homes(){
        return view('admin.homes');
    }
}
