<?php

namespace App\Http\Controllers\Admin;

use App\Categories;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class CategoriesController extends MainController
{
    public function categories(){
        $categoriess = Categories::all();
        return view('admin.categories.categories', compact('categoriess'));
    }

    public function create_product($categories_id){

        $slid = Input::file('images');
        $dir = '/images/product/';

        do {
            $filename = str_random(30).'.'.$slid->getClientOriginalExtension();
        } while (File::exists(public_path().$dir.$filename));

        Input::file('images')->move(public_path().$dir, $filename);

        Product::create(['categories_id' => $categories_id,'title' => Input::get('title'),'body' => Input::get('body'), 'images' => $dir.$filename,]);
        return redirect(route('admin.categories.show_products', ['categories_id' => $categories_id]));
    }

    public function show_products($categories_id){
        $categors = Product::whereCategoriesId($categories_id)->get();
        return view('admin.categories.show_products', compact('categors','categories_id'));
    }

    public function update_product($id, Request $request){

        $product = Product::findOrFail($id);
        $categories_id = $product->categories_id;
        $product->update($request->except('images'));
        $slid = Input::file('images');
        if ($slid != null) {
            $dir = '/images/product/';

            do {
                $filename = str_random(30) . '.' . $slid->getClientOriginalExtension();
            } while (File::exists(public_path() . $dir . $filename));

            Input::file('images')->move(public_path() . $dir, $filename);
            $product->update(['images' => $filename]);
        }


        return redirect(route('admin.categories.show_products', ['categories_id' => $categories_id]));
    }

    public function destroy_show($id){
        Product::whereId($id)->delete();
        return redirect()->back();
    }
}
