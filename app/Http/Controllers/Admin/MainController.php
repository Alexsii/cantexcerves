<?php

namespace App\Http\Controllers\Admin;

use App\Addres;
use App\Categories;
use App\Email;
use App\Http\Requests\CreatephoneRequest;
use App\Logo;
use App\Message;
use App\Phone_number;
use App\Product;
use App\Slider;
use App\Social;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $i = Message::whereStatus(0)->count();

        view()->share(compact('i'));
    }
    public function index(){
        return view('admin.index');
    }
    public function panels(){
        return view('admin.panels');
    }
    public function login(){
        return view('auth.login');
    }


    public function address(){
        $address = Addres::all();
        return view('admin.address.address', compact('address'));
    }
    public function edit_address($id){
        $addres = Addres::findOrFail($id);
        return view('admin.address.edit_address', compact('addres'));
    }

    public function update_addres($id, Request $request){
        $addres = Addres::findOrFail($id);
        $addres->update($request->all());

        $address = Addres::all();
        return view('admin.address.address', compact('address'));
    }


    public function messages(){
        $not_viewed = '0';
        $viewed = '1';
        $messagess = Message::whereStatus($not_viewed)->get();
        $messags = Message::whereStatus($viewed)->get();
        return view('admin.message.messages', compact('messagess', 'messags'));
    }
    public function show_new_messege($id){
        $viewes = Message::whereId($id)->first();
        $viewes->update(['status' => '1']);
        $viewess = Message::whereId($id)->get();
        return view('admin.message.show_messages', compact('viewess'));
    }
    public function look_old_messeges($id){

        $viewess = Message::whereId($id)->get();
        return view('admin.message.show_messages', compact('viewess'));
    }

    public function destroy_message($id){
        Message::whereId($id)->delete();
        return redirect()->back();
    }

    public function settings(){
        $emailss = Email::all();
        $numbers = Phone_number::all();
        $socials = Social::all();
        $logos = Logo::all();
        return view('admin.setting.settings', compact('logos','socials','emailss','numbers'));
    }

    public function audio(){
        return view('admin.radio');
    }
}
