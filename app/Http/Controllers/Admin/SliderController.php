<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Slider;
use App\Social;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class SliderController extends MainController
    {

    /*                                    641641614 646 6 6 61 6 6 16 146 16 6-*/
    public function slider(){
        $slid = Slider::first(['pach']);
        $sliders = Slider::all();
        $socials = Social::all();
        return view('admin.create.slider', compact('slid','sliders','socials'));
    }
    public function stor(){

        $slid = Input::file('pach');
        $dir = '/uploads/slider/';

        do {
            $filename = str_random(30).'.'.$slid->getClientOriginalExtension();
        } while (File::exists(public_path().$dir.$filename));

        Input::file('pach')->move(public_path().$dir, $filename);

        Slider::create(['pach' => $dir.$filename]);
        return redirect('/');
    }
    public function edit(Slider $slider){
        return view('admin.create.edit', compact('slider'));
    }
    public function update($id){

        $slider = Slider::findOrFail($id);
        File::delete(public_path().$slider->pach);

        $slid = Input::file('pach');
        $dir = '/uploads/slider/';

        do {
            $filename = str_random(30).'.'.$slid->getClientOriginalExtension();
        } while (File::exists(public_path().$dir.$filename));

        Input::file('pach')->move(public_path().$dir, $filename);

        $slider->update(['pach' => $dir.$filename]);

        $sliders = Slider::all();
        $slid = Slider::first(['pach']);
        return view('admin.create.slider', compact('sliders','slid'));
    }
    public function destroy($id){
        $slider = Slider::findOrFail($id);
        File::delete(public_path().$slider->pach);
        Slider::whereId($id)->delete();
        return redirect()->back();
    }
    /*                                    641641614 646 6 6 61 6 6 16 146 16 6-*/
}
