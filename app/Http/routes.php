<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@home');
Route::get('/products','HomeController@products');
Route::get('/products/air/{categories_id}','HomeController@air');
Route::POST('/stor','HomeController@stor');
Route::get('/contact','HomeController@contact');


Route::group(['prefix' => 'admin' , 'namespace' => 'Admin'], function () {
Route::get('/', 'MainController@index');
Route::get('/panels','MainController@panels');
Route::get('/login','MainController@login');


Route::get('/slider','SliderController@slider');
Route::post('/slider', 'SliderController@stor');
Route::get('/slider/{slider}/edit', 'SliderController@edit')->name('admin.slider.edit');
Route::get('/slider/{id}/destroy', 'SliderController@destroy');
Route::PATCH('/slider/{id}', 'SliderController@update');


Route::get('/address','MainController@address');
Route::get('/address/{id}/edit_address','MainController@edit_address');
Route::PATCH('/address/{id}', 'MainController@update_addres');


Route::PATCH('settings/edit_numbers/{id}', 'SettingsController@updates');
Route::PATCH('/settings/emails/{id}', 'SettingsController@update_emails');
Route::PATCH('/settings/social/{id}', 'SettingsController@update_social');
Route::PATCH('/settings/logo/{id}', 'SettingsController@update_logo');


Route::get('/categories','CategoriesController@categories');
Route::post('/categories/create_product/{categories_id}','CategoriesController@create_product');
Route::get('/categories/{categories_id}','CategoriesController@show_products')->name('admin.categories.show_products');
Route::PATCH('/categories/{id}','CategoriesController@update_product');
Route::get('/categories/{id}/destroy','CategoriesController@destroy_show');


Route::get('/messages', 'MainController@messages');
Route::get('/messages/{id}/show_new_messege', 'MainController@show_new_messege');
Route::get('/messages/{id}/look_old_messeges', 'MainController@look_old_messeges');
Route::get('/messages/{id}', 'MainController@destroy_message');

Route::get('/settings', 'MainController@settings');
Route::get('/audio','MainController@audio');

Route::get('/home','SettingsController@homes');
});
Route::auth();

Route::get('/home', 'HomeController@index');
