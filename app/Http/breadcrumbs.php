<?php

// Home
use App\Categories;

Breadcrumbs::register('welcome', function($breadcrumbs)
{
    $breadcrumbs->push('Главная', url('/'));
});

// Home > About
Breadcrumbs::register('products', function($breadcrumbs)
{
    $breadcrumbs->parent('welcome');
    $breadcrumbs->push('/ Продукция', url('products'));
});
Breadcrumbs::register('contact', function($breadcrumbs)
{
    $breadcrumbs->parent('welcome');
    $breadcrumbs->push('/ Контакты', url('contact'));
});
// Home > Blog
Breadcrumbs::register('2', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('/  Тепловые насосы воздух/вода', url('2'));
});
Breadcrumbs::register('5', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('/ Водонагреватели (бойлеры)', url('5'));
});
Breadcrumbs::register('1', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('/ Тепловые насосы геотермальные', url('1'));
});
Breadcrumbs::register('3', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('/ Тепловые насосы вентиляционные', url('3'));
});
Breadcrumbs::register('4', function($breadcrumbs)
{
    $breadcrumbs->parent('products');
    $breadcrumbs->push('/ твердотопливные котлы', url('4'));
});
// Home > Blog > [Category]
