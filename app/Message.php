<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'id',
        'name_users',
        'email_users',
        'messages',
        'pablished_at',
        'status'
        ];
}
