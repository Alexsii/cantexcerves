<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [
      'id','categories_id','title', 'body', 'images'
    ];
}
