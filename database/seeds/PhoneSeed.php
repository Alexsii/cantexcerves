<?php

use App\Addres;
use App\Email;
use App\Phone_number;
use Illuminate\Database\Seeder;

class PhoneSeed extends Seeder
{
    /**
     *
     */
    public function run()
    {
        Addres::create(['addres' => '02222 г. Киев ул. Пуховская 4']);
        Email::create(['email' => 'v4030504@gmail.com']);
        Email::create(['email' => 'ivt4030504@gmail.com']);
        Phone_number::create(['phone' => '(044)361 02 75']);
        Phone_number::create(['phone' => '(067)403 05 04']);
        Phone_number::create(['phone' => '(095)508 20 26']);
    }
}
