<?php

use Illuminate\Database\Seeder;

class DateBPhoneSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PhoneSeed::class);
    }
}
