<?php

use App\Addres;
use App\Categories;
use App\Email;
use App\Phone_number;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::create(['categories_id' => '1','title' => 'Тепловые насосы геотермальные', 'link' => 'geotermal']);
        Categories::create(['categories_id' => '2','title' => 'Тепловые насосы воздух/вода', 'link' => 'air']);
        Categories::create(['categories_id' => '3','title' => 'Тепловые насосы вентиляционные', 'link' => 'ventilation']);
        Categories::create(['categories_id' => '4','title' => 'Твердотоплевные котлы', 'link' => 'fuel']);
        Categories::create(['categories_id' => '5','title' => 'Водонагреватели (бойлеры)', 'link' => 'bolier']);
    }
}
