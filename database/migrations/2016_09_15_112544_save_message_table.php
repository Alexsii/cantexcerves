<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SaveMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('messages', function($table)
       {
           $table->increments('id');
           $table->string('name_users');
           $table->string('email_users');
           $table->string('messages');
           $table->timestamps();
           $table->timestamp('pablished_at')->nullable();
           $table->string('status');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
